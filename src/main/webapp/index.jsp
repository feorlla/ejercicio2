<%-- 
    Document   : index
    Created on : Apr 1, 2021, 7:20:07 PM
    Author     : GorillaSetups
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="form" action="CalculadoraControlador" method="GET">
            <label for="valor1">ingrese valor 1: </label>
            <input type="text" id="valor1" name="valor1">
            
            <label for="valor2">ingrese valor 2: </label>
            <input type="text" id="valor2" name="valor2">
            
            <button type="submit" class="btn btn-succes">Resultado</button>
        </form>
    </body>
</html>
