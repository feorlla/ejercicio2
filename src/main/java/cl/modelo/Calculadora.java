/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.modelo;

/**
 *
 * @author GorillaSetups
 */
public class Calculadora {
    private int valor1;
    private int valor2;
    private int resultadosuma;

    public int getValor1() {
        return valor1;
    }

    public void setValor1(int valor1) {
        this.valor1 = valor1;
    }

    public int getValor2() {
        return valor2;
    }

    public void setValor2(int valor2) {
        this.valor2 = valor2;
    }

    public int getResultadosuma() {
        
        return this.valor1+this.valor2;
    }
    

    public void setResultadosuma(int resultadosuma) {
        this.resultadosuma = resultadosuma;
    }
    
    
    
}
